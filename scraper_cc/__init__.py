from os.path import abspath
from openpyxl import Workbook
from selenium.common.exceptions import UnexpectedAlertPresentException, NoAlertPresentException

from minsplinter import MinSplinter


work_dir = abspath('.')

initial_url = 'http://www.county-clerk.net/county.asp?state=Oklahoma'

minsplinter_conf = {
	'DRIVER_NAME': 'firefox',
	'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
	'Chrome/58.0.3029.110 Safari/537.36'
}

default_wait_time = 3


def _remove_alerts(sp_obj):
	"""Remove alerts

	Args:
		sp_obj (MinSplinter): Ovject to browser interface.

	Returns:
		None
	"""
	try:
		alert = sp_obj.browser.get_alert()
		if alert is not None:
			alert.accept()
	except NoAlertPresentException:
		pass


def _get_countries(sp_obj):
	"""list: Get countries

	Extract COUNTIES from site.

	Args:
		sp_obj (MinSplinter): Ovject to browser interface.

	Returns:
		list: List contains sub-list with pairs of County name and url to extract more data.
	"""
	countries = list()
	s = '#statelist > li > a'
	el = sp_obj.find_elements('css', s)
	i = 0
	while i < len(el):
		countries.append([el[i].text, el[i]['href']])
		i += 1
	return countries


def _get_country_data(sp_obj, obj):
	"""bool: Get county data

	Args:
		sp_obj (MinSplinter): Ovject to browser interface.
		obj (dict): Object to save extracted data too. Existing values in dict. will be overwritten.

	Returns:
		bool: True if data extraction succeeded, False otherwise.
	"""
	s = 'div.content > h3'
	s1 = 'div.content > div.info'

	el = sp_obj.find_elements('css', s)
	el1 = sp_obj.find_elements('css', s1)

	if len(el) != len(el1):
		return False

	required_fields = ['Phone', 'County Website']

	for rf in required_fields:
		try:
			i = 0
			while i < len(el) and el[i].text != rf:
				i += 1

			if i < len(el):
				v = el1[i].text
				if rf == 'County Website':
					s2 = 'a'
					el2 = sp_obj.find_elements('css', s2, el1[i])
					v = ''
					if len(el2):
						v = el2[0]['href']

				obj[el[i].text] = v
		except IndexError:
			pass
	return True


def scraper():
	"""Scr<per

	Main scraper method.

	Returns:
		None
	"""
	wb = Workbook()
	ws = wb.active

	ws.append(['County', 'Phone', 'County Website'])

	sp_obj = MinSplinter(minsplinter_conf)
	sp_obj.visit(initial_url)
	sp_obj.wait()
	countries = _get_countries(sp_obj)

	i = 0
	while i < len(countries):
		_remove_alerts(sp_obj)
		sp_obj.visit(countries[i][1])
		sp_obj.wait(default_wait_time)

		repeat = True
		while repeat:
			try:
				o = dict()
				o.update({
					'County': countries[i][0],
					'Phone': '',
					'County Website': ''
				})
				_get_country_data(sp_obj, o)
				ws.append([o['County'], o['Phone'], o['County Website']])
				repeat = False
			except UnexpectedAlertPresentException:
				_remove_alerts(sp_obj)
		i += 1

	sp_obj.quit()
	filename = work_dir + '/output.xlsx'
	wb.save(filename=filename)
